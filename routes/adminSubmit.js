import { Router } from 'express';
import { insertJaratok } from '../database/jaratok.js';

const router = Router();

router.post('/', async (req, resp) => {
  try {
    await insertJaratok(req.body.adminFrom, req.body.adminWhere, req.body.adminDay,
      req.body.adminTime, req.body.adminPrice, req.body.trainType);
    resp.redirect('/');
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

export default router;
