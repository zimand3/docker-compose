import { Router } from 'express';
import { deleteJaratok } from '../database/jaratok.js';
import { deleteFoglalas } from '../database/foglalas.js';

const router = Router();

router.get('/:id', async (req, resp) => {
  const jId = req.params.id;
  try {
    await deleteJaratok(jId);
    // const jaratok = await jrt.selectJaratok();
    resp.redirect('/');
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const result = await deleteFoglalas(req.params.id);
    if (result.affectedRows !== 0) {
      const sendMessage = 'Deleted succesfully!';
      res.end(sendMessage);
    } else {
      const sendMessage = 'Reservation already deleted!';
      res.status(404);
      res.end(sendMessage);
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
});

export default router;
