import { Router } from 'express';
import { selectJaratok } from '../database/jaratok.js';
import { selectReviews } from '../database/reviews.js';

const router = Router();

router.get('/', async (req, resp) => {
  try {
    const jaratok = await selectJaratok();
    const revs = await selectReviews();
    const error = '';
    resp.render('homepage', { error, jaratok, revs });
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

export default router;
