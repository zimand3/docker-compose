import { Router } from 'express';
import { registerUser }  from '../database/felhasznalo.js';

const router = Router();

router.post('/', async (req, res) => {
  try {
    await registerUser(req.body.rgUsername, req.body.rgPassword, '0', req.body.rgFirstName, req.body.rgLastName, req.body.rgMail);
    res.redirect('/login');
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
});

export default router;
