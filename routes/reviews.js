import { Router } from 'express';
import { insertReview, deleteReview } from '../database/reviews.js';

const router = Router();

router.post('/', async (req, resp) => {
  try {
    await insertReview(resp.locals.payload.uid, req.body.revTextArea, resp.locals.payload.uname);
    resp.redirect('/');
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

router.delete('/:id', async (req, resp) => {
  try {
    const res = await deleteReview(req.params.id);
    if (res.affectedRows !== 0) {
      const message = 'Deleted succesfully!';
      resp.end(message);
    } else {
      const message = 'Review does not exist or was deleted';
      res.status(404);
      res.end(message);
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

export default router;
