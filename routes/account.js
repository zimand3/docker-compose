import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import fs from 'fs';
import path from 'path';
import { Router } from 'express';
import { myFoglalasok, ticketValability, priceOfTicket } from '../database/foglalas.js';
import { updateMoney, ticketInfo } from  '../database/felhasznalo.js';

const router = Router();

router.get('/', async (req, res) => {
  try {
    const foglalasok = await myFoglalasok(res.locals.payload.uid);
    res.render('account', { foglalasok });
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
});

router.post('/addMoney', async (req, res) => {
  try {
    await updateMoney(100, res.locals.payload.uid);
    const resp = res.locals.payload.wallet + 100;
    res.json(resp);
  } catch (err) {
    res.status(402);
    res.end();
  }
});

router.post('/buyTicket/:id', async (req, res) => {
  try {
    const fid = req.params.id;
    const obj = await priceOfTicket(fid);
    if (obj[0].price > res.locals.payload.wallet) {
      const error = 'TopUp some money to buy this ticket!';
      res.render('error', { error });
    } else {
      await ticketValability(1, fid);
      await updateMoney(-obj[0].price, res.locals.payload.uid);
      const info = await ticketInfo(fid);
      const content = fs
        .readFileSync(path.resolve('./static', 'ticket.docx'), 'binary');
      const zip = new PizZip(content);
      let doc;
      try {
        doc = new Docxtemplater(zip);
      } catch (err) {
        const error = `Something went wrong: ${err}`;
        res.render('error', { error });
      }
      doc.setData({
        RESID: `000${fid}`,
        TCKDEST: `${info[0].destination}`,
        TCKDEP: `${info[0].departure}`,
        NAME: `${info[0].fname} ${info[0].lname}`,
        MAIL: `${info[0].mail}`,
      });
      try {
        doc.render();
      } catch (err) {
        const error = `Something went wrong: ${err}`;
        res.render('error', { error });
      }
      const buf = doc.getZip()
        .generate({ type: 'nodebuffer' });
      fs.writeFileSync(path.resolve('./files', `ticket_${fid}.docx`), buf);
      const error = 'You can check your ticket in your account!';
      res.render('error', { error });
    }
  } catch (err) {
    const error = 'Something went wrong, try again';
    res.render('error', { error });
  }
});

export default router;
