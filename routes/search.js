import { Router } from 'express';
import { searchJaratok } from '../database/jaratok.js';
import { selectReviews } from '../database/reviews.js';

const router = Router();

router.get('/', async (req, resp) => {
  try {
    const error = '';
    let min = '0';
    let max = '99999';
    let date = '%';
    let traintype = '%';
    if (req.query.minprice !== '') {
      min = req.query.minprice;
    }
    if (req.query.maxprice !== '') {
      max = req.query.maxprice;
    }
    if (req.query.searchDate !== '') {
      date = req.query.searchDate;
    }
    if (req.query.searchtt !== '') {
      traintype = req.query.searchtt;
    }
    const revs = await selectReviews();
    const jaratok = await searchJaratok(req.query.guestFrom,
      req.query.guestWhere, min, max, date, traintype);
    resp.render('homepage', { error, jaratok, revs });
  } catch (e) {
    const error = `Something went wrong: ${e}`;
    resp.render('error', { error });
  }
});

export default router;
