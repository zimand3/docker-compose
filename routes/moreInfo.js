import { Router } from 'express';
import {  getMoreInfo } from '../database/jaratok.js';

const router = Router();

router.get('/:id', async (req, resp) => {
  try {
    const responseText = await getMoreInfo(req.params.id);
    if (Object.entries(responseText).length === 0) {
      resp.status(404);
      resp.end();
    } else {
      resp.json(responseText);
      resp.end();
    }
  } catch (error) {
    resp.status(405);
    resp.end();
  }
});

export default router;
