import { Router } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { getLogin } from '../database/felhasznalo.js';
import secret from  '../utils/utils.js';

const router = Router();

router.get('/', (req, resp) => {
  resp.render('login');
});

router.get('/register', (req, resp) => {
  resp.render('register');
});

router.post('/login', async (req, resp) => {
  try {
    const uname = req.body.loginUsername;
    const login = await getLogin(uname);
    if (Object.keys(login).length === 0) {
      const error = 'Incorrect credentials!';
      resp.render('error', { error });
      return;
    }
    // const hash = bcrypt.hashSync(login[0].password, 10);
    if (await bcrypt.compare(req.body.loginPassword, login[0].password)) {
      const kuki = jwt.sign({ uname }, secret);
      resp.cookie('kuki', kuki, {
        httpOnly: true,
        sameSite: 'strict',
      });
      resp.redirect('/');
    } else {
      resp.status(401);
      const error = 'Incorrect credentials!';
      resp.render('error', { error });
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

router.post('/logout', (req, res) => {
  res.clearCookie('kuki');
  res.redirect('/login');
});

export default router;
