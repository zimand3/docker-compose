import { Router } from 'express';
import { selectFoglalas, insertFoglalas } from '../database/foglalas.js';
import { selectFelhasznaloIds } from '../database/felhasznalo.js';

const router = Router();

router.get('/:id', async (req, resp) => {
  try {
    const jId = req.params.id;
    const foglalasok = await selectFoglalas(jId);
    const felhasznalok = await selectFelhasznaloIds();
    resp.render('foglalasok', { felhasznalok, jId, foglalasok });
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

router.post('/getFoglalasok/foglalas/:id', async (req, resp) => {
  try {
    await insertFoglalas(req.params.id, resp.locals.payload.uid);
    const jId = req.params.id;
    resp.redirect(`/getFoglalasok/${jId}`);
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    resp.render('error', { error });
  }
});

export default router;
