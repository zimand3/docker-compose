import { Router } from 'express';

const router = Router();

router.get('/', async (req, resp) => {
  const error = '';
  resp.render('admin', { error });
});

export default router;
