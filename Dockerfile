# Specify which docker with nodeJS version you want
FROM node:14.15.5

# Create working directory
WORKDIR /usr/src/zdim1981

# Copy dependencies file
COPY package.json .
# and install the dependencies (without the development deps)
RUN npm install --production

# Copy all the directories which are part of the application (see .dockerignore)
COPY . .

# The port 3000 is hardcoded for the server so we need to expose that port
# expose = The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.
# see https://docs.docker.com/engine/reference/builder/
EXPOSE 3000

# Start the application using this command
CMD ["node", "index"]