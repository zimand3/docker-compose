import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import adminSubmit from './routes/adminSubmit.js';
import admin from './routes/admin.js';
import guest from './routes/search.js';
import render from './routes/render.js';
import del from './routes/delete.js';
import getFoglalasok from './routes/foglalasok.js';
import  getMoreInfo from './routes/moreInfo.js';
import  log from './routes/auth.js';
import review from './routes/reviews.js';
import register from './routes/register.js';
import account from './routes/account.js';
import checkJWT,  {
  whoIsLoggedIn, checkAdmin, canDelete, canRegister,  canDelRev,
} from './middleware/auth.js';
import searchFormCheck, { adminFormCheck, registerCheckout } from './middleware/forms.js';

const staticDir = path.join(process.cwd(), 'static');
const filesDir = path.join(process.cwd(), 'files');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(cookieParser());
app.use(whoIsLoggedIn);
app.use(express.static(staticDir));
app.use(express.static(filesDir));
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use('/adminFormSuibmitted', checkAdmin, adminFormCheck, adminSubmit);
app.use('/admin', checkAdmin,  admin);
app.use('/guestFormSubmitted', searchFormCheck, guest);
app.use('/', render);
app.use('/getFoglalasok', checkJWT,  getFoglalasok);
app.use('/deletJarat', checkAdmin, del);
app.use('/deleteFoglalas', canDelete, del);
app.use('/getInfo', getMoreInfo);
app.use('/login', log);
app.use('/submitrgs', canRegister, registerCheckout,  register);
app.use('/manageAcc', checkJWT, account);
app.use('/addReview', checkJWT, review);
app.use('/review', canDelRev, review);

app.listen(PORT, () => {
  console.log(`Server listening on PORT: ${PORT}`);
});
