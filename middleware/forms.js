import {  selectJaratok } from '../database/jaratok.js';
import { selectReviews } from '../database/reviews.js';

const nameRegex = new RegExp('^[A-Z][a-z]+');
const numRegex = new RegExp('[0-9]*');
const trainTypes = ['Long-Distance', 'High-Speed',
  'Inter-City', 'Regional', 'Short-Distance', 'Commuter', 'Rapid-Transit', 'Tram'];
const timeRegex = new RegExp('[0-2][0-9]:[0-5][0-9]');
const dateRegex = new RegExp('20[0-9][0-9]-[0-1][0-9]-[0-3][0-9]');
// eslint-disable-next-line no-useless-escape
const mailRegex = new RegExp('^[a-zA-Z0-9.!#$%&*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]{2,4})*$');

export default async function searchFormCheck(req, res, next) {
  let error = '';
  if (!nameRegex.test(req.query.guestFrom)) {
    error += 'Invalid Departure!\n';
  }
  if (!nameRegex.test(req.query.guestWhere)) {
    error += 'Invalid Destination!\n';
  }
  if (!numRegex.test(req.query.minPrice) && req.query.minPrice !== '') {
    error += 'Invalid Minimum Price!\n';
  }
  if (!numRegex.test(req.query.minPrice) && req.query.minPrice !== '') {
    error += 'Invalid Maximum Price!\n';
  }
  if (error === '') {
    res.status(200);
    next();
  } else {
    const jaratok = await selectJaratok();
    const revs = await selectReviews();
    res.render('homepage', { error, jaratok, revs });
  }
}

export function adminFormCheck(req, res, next) {
  let error = '';
  if (!nameRegex.test(req.body.adminFrom)) {
    error += 'Invalid Departure!\n';
  }
  if (!nameRegex.test(req.body.adminWhere)) {
    error += 'Invalid Destination!\n';
  }
  if (!numRegex.test(req.body.adminPrice)) {
    error += 'Invalid Price!\n';
  }
  if (!timeRegex.test(req.body.adminTime)) {
    error += 'Invalid Time!\n';
  }
  if (!dateRegex.test(req.body.adminDay)) {
    error += 'Invalid date!\n';
  }
  if (!trainTypes.includes(req.body.trainType)) {
    error += 'Invalid train type!\n';
  }
  if (error === '') {
    next();
  } else {
    res.render('admin', { error });
  }
}

export function registerCheckout(req, res, next) {
  let error = '';
  if (!nameRegex.test(req.body.rgFirstName)) {
    error += 'Invalid First Name!\n';
  }
  if (!nameRegex.test(req.body.rgLastName)) {
    error += 'Invalid Last Name!\n';
  }
  if (!mailRegex.test(req.body.rgMail)) {
    error += 'Invalid Email!\n';
  }
  if (error === '') {
    next();
  } else {
    res.render('error', { error });
  }
}
