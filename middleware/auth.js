import jwt from 'jsonwebtoken';
import { existsUser } from '../database/felhasznalo.js';
import { getUser } from '../database/foglalas.js';
import secret from  '../utils/utils.js';
import { selectReviewById } from '../database/reviews.js';

export async function whoIsLoggedIn(req, res, next) {
  res.locals.payload = {};
  try {
    if (req.cookies.kuki) {
      try {
        res.locals.payload = await jwt.verify(req.cookies.kuki, secret);
        const rank = await existsUser(res.locals.payload.uname);
        res.locals.payload.isAdmin = rank[0].isAdmin;
        res.locals.payload.uid = rank[0].userID;
        res.locals.payload.fname = rank[0].fname;
        res.locals.payload.lname = rank[0].lname;
        res.locals.payload.mail = rank[0].mail;
        res.locals.payload.wallet = rank[0].wallet;
        next();
      } catch (err) {
        const error = `Something went wrong: ${err}`;
        res.render('error', { error });
      }
    } else {
      next();
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
}

export default async function checkJWT(req, res, next) {
  if (req.cookies.kuki) {
    try {
      next();
    } catch (error) {
      res.clearCookie('kuki');
      res.status(401).end();
    }
  } else {
    const error = 'Login to access this feature!';
    res.status(401);
    res.render('error', { error });
  }
}

export function checkAdmin(req, res, next) {
  if (res.locals.payload.isAdmin === 1) {
    next();
  } else {
    const error = 'Unauthorised user!';
    res.status(401);
    res.render('error', { error });
  }
}

export async function canDelRev(req, res, next) {
  const user = await selectReviewById(req.url.substring(1));
  if (res.locals.payload.isAdmin === 1 || user[0].reviewername === res.locals.payload.uname) {
    next();
  } else {
    const error = 'Unauthorised user!';
    res.status(401);
    res.render('error', { error });
  }
}

export async function canDelete(req, res, next) {
  try {
    const uid = await getUser(req.url.substring(1));
    if (res.locals.payload.uid === uid[0].userID) {
      next();
    } else {
      const error = 'Can not delet other user reservation';
      res.render('error', { error });
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
}

export async function canRegister(req, res, next) {
  try {
    const user = await existsUser(req.body.rgUsername);
    if (Object.keys(user).length === 0) {
      next();
    } else {
      const error = 'Username taken!';
      res.status(409);
      res.render('error', { error });
    }
  } catch (err) {
    const error = `Something went wrong: ${err}`;
    res.render('error', { error });
  }
}
