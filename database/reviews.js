import { qp } from './database.js';

export const insertReview = async (uid, text, revname) => {
  const query = 'insert into Reviews(revtext, reviewerid, reviewername) values(?, ?, ?)';
  return qp(query, [text, uid, revname]);
};

export const selectReviews = async () => {
  const query = 'select * from Reviews';
  return qp(query);
};

export const deleteReview = async (id) => {
  const query = 'delete from Reviews where revid = ?';
  return qp(query, [id]);
};

export const selectReviewById = async (id) => {
  const query = 'select * from Reviews where revid = ?';
  return qp(query, [id]);
};
