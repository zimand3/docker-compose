import { qp } from './database.js';

export const insertFoglalas = async (jaratID, userID) => {
  const query = 'insert into Foglalas(jaratID, userID) values(?, ?)';
  return qp(query, [jaratID, userID]);
};

export const selectFoglalas = async (jaratID) => {
  const query = 'select * from Foglalas where jaratID = ?';
  return qp(query, [jaratID]);
};

export const deleteFoglalas = async (foglalID) => {
  const query = 'delete from Foglalas where foglalasID = ?';
  return qp(query, [foglalID]);
};

export const getUser = async (id) => {
  const query =  'select userID from Foglalas where foglalasID = ?';
  return qp(query, [id]);
};

export const myFoglalasok = async (id) => {
  const query =  'select * from Foglalas where userID = ?';
  return qp(query, [id]);
};

export const ticketValability = async (val, id) => {
  const query =  'update Foglalas set valability = ? where foglalasID = ?';
  return qp(query, [val, id]);
};

export const priceOfTicket = async (id) => {
  const query =  `select Jaratok.price
    from Jaratok
    join Foglalas
    on Foglalas.JaratID = Jaratok.JaratID
    where Foglalas.FoglalasID = ?`;
  return qp(query, [id]);
};
