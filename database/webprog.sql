--Az alabbi kodreszlet inicializalja az adatbazist, es a root usernek teljes jogokat ad
--MYSQL workbenchen futtattam, ott sorrol sorra futott csak le, ugyhogy teszteleshez ajanlom ugyanezt
--Mindenek elott ezze a fileal kell az adatbazist inicializalni
--Ahhoz, hogy futatni lehessen, szuksegunk lesz mysqlre
create DATABASE webprog if not exists;
select webprog;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'web2021'
flush privileges;

--Ha megvan az adatbazis akkor a kovetkezo url-ekre kell rakeresni:
--http://localhost:5050/    -> fooldal minden aktualis jarattal es keresei formmal, innen gombokkal megy a navigacio
-- node utils/init.js beszur 2 felhasznalot es 1 admint, az SQL futtatasa utan ezt is le kell kulon futtatni