import { qp } from './database.js';

export const insertJaratok = async (dep, dest, day, time, price, type) => {
  const query = 'insert into Jaratok(departure, destination, day, time, price, trainType) values (?, ?, ?, ?, ?, ?);';
  return qp(query, [dep, dest, day, time, price, type]);
};

export const deleteJaratok = async (id) => {
  const query = 'delete from Jaratok where jaratID = ?;';
  return qp(query, [id]);
};

export const selectJaratok = async () => {
  const query = 'select * from Jaratok';
  return qp(query);
};

export const searchJaratok = async (dep, dest, minprice, maxprice, date, tt) => {
  const query = 'select * from Jaratok where departure = ? and destination = ? and price >= ? and price <= ? and day like ? and trainType like ?';
  return qp(query, [dep, dest, minprice, maxprice, date, tt]);
};

export const getJaratIds = async () => {
  const query = 'select jaratID from Jaratok';
  return qp(query);
};

export const getJaratById = async (id) => {
  const query = 'select * from Jaratok where jaratID = ?';
  return qp(query, [id]);
};

export const getMoreInfo = async (id) => {
  const query = 'select price, traintype from Jaratok where jaratID = ?';
  return qp(query, [id]);
};
