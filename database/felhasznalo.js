import bcrypt from 'bcrypt';
import { qp } from './database.js';

export const registerUser = async (un, pw, ia, fn, ln, mail) => {
  const query = 'insert into Felhasznalo(felhasznalonev, password, isAdmin, fname, lname, mail) values(?, ?, ?, ?, ?, ?)';
  const hash = bcrypt.hashSync(pw, 10);
  return qp(query, [un, hash, ia, fn, ln, mail]);
};

export const selectFelhasznaloNames = async () => {
  const query = 'select firstName, lastName from Felhasznalo';
  return qp(query);
};

export const selectFelhasznaloIds = async () => {
  const query = 'select userID from Felhasznalo';
  return qp(query);
};

export const getLogin = async (name, password) => {
  const query = 'select password from Felhasznalo where felhasznalonev = ?';
  return qp(query, [name, password]);
};

export const getRank = async (username) => {
  const query = 'select isAdmin, userID from Felhasznalo where felhasznalonev = ?';
  return qp(query, [username]);
};

export const existsUser = async (username) => {
  const query = 'select * from Felhasznalo where felhasznalonev = ?';
  return qp(query, [username]);
};

export const updateMoney = async (val, uid) => {
  const query = 'update Felhasznalo set wallet = wallet + ? where userID = ?';
  return qp(query, [val, uid]);
};

export const ticketInfo = async (id) => {
  const query = `select Felhasznalo.fname, Felhasznalo.lname, Jaratok.destination, Jaratok.departure, Jaratok.jaratID, Felhasznalo.mail 
    from Felhasznalo
    join Foglalas 
    on Foglalas.userID = Felhasznalo.userID
    join Jaratok
    on Jaratok.jaratID = Foglalas.jaratID
    where Foglalas.foglalasID = ?`;
  return qp(query, [id]);
};
