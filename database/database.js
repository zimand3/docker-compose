import mysql from 'mysql';
import util from 'util';

export const pool = mysql.createPool({
  database: 'webprog',
  host: 'host.docker.internal',
  user: 'root',
  password: 'root',
  connectionLimit: 5,
});

export const qp = util.promisify(pool.query).bind(pool);

export const createDataBase = async () => {
  pool.query(`create table if not exists Jaratok  (
    jaratID int auto_increment primary key,
    departure varchar(30),
    destination varchar(30),
    day date,
    time varchar(10),
    price int,
    traintype varchar(30)
  );`, (error) => {
    if (error) {
      console.error(`Create table error: ${error.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully: Jaratok!');
    }
  });

  pool.query(`create table if not exists Felhasznalo (
    userID int auto_increment primary key, 
    felhasznalonev varchar(30),
    fname varchar(30),
    lname varchar(30),
    mail varchar(50),
    wallet double default 0.00,
    password varchar(60),
    isAdmin int
  );`, (error) => {
    if (error) {
      console.error(`Create table error: ${error.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully: Felhasznalo!');
    }
  });

  pool.query(`create table if not exists Foglalas (
    foglalasID int auto_increment primary key,
    jaratID int,
    userID int,
    valability int default 0,
    foreign key (userID) references Felhasznalo(userID) on delete cascade,
    foreign key (jaratID) references Jaratok(jaratID) on delete cascade
  );`, (error) => {
    if (error) {
      console.error(`Create table error: ${error.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully: Foglalas!');
    }
  });

  pool.query(`create table if not exists Reviews (
    revid int auto_increment primary key,
    revtext varchar(255),
    reviewerid int,
    reviewername varchar(50),
    foreign key (reviewerid) references Felhasznalo(userID) on delete cascade
  );`, (error) => {
    if (error) {
      console.error(`Create table error: ${error.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully: Reviews!');
    }
  });
};

createDataBase();
