// eslint-disable-next-line no-unused-vars
const moreInfo = async (itemID) => {
  try {
    const response = await fetch(`/getInfo/${itemID}`);
    const obj = await response.json();
    if (response.status !== 200) {
      document.getElementById(`errdiv_${itemID}`).innerText = 'Can not get more info!';
    } else {
      const toSend = `Price: ${obj[0].price}, Type: ${obj[0].traintype}`;
      document.getElementById(`moreInfo_${itemID}`).innerText = toSend;
    }
  } catch (err) {
    document.getElementById(`errdiv_${itemID}`).innerText = 'Can not get more info! Route might not be available!';
  }
};

// eslint-disable-next-line no-unused-vars
async function rmFoglalas(foglalasID) {
  await fetch(`/deleteFoglalas/${foglalasID}`, {
    method: 'DELETE',
  })
    .then((response) => response.text())
    .then((text) => {
      document.getElementById(`foglalas_${foglalasID}`).remove();
      document.getElementById('rmResp').innerText = text;
    })
    .catch((error) => {
      document.getElementById('rmCatch').innerText = error;
    });
}

// eslint-disable-next-line no-unused-vars
async function rmReview(id) {
  await fetch(`review/${id}`, {
    method: 'DELETE',
  })
    .then((response) => response.text())
    .then((text) => {
      document.getElementById(`reviewNumber${id}`).remove();
      document.getElementById('revresp').innerText = text;
    })
    .catch((error) => {
      document.getElementById('revcatch').innerText = error;
    });
}

// eslint-disable-next-line no-unused-vars
async function addMoney() {
  try {
    const resp = await fetch('/manageAcc/addMoney/', {
      method: 'POST',
    });
    const obj = await resp.json();
    if (resp.status !== 200) {
      document.getElementById('topuperr').innerText = 'TopUp Error';
    } else {
      document.getElementById('myWallet').innerText = `${obj}$`;
    }
  } catch (err) {
    document.getElementById('topuperr').innerText = 'TopUp Catch Error';
  }
}

// eslint-disable-next-line no-unused-vars
function showForm() {
  document.getElementById('extra').style.display = 'block';
}
